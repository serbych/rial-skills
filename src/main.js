import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VModal from 'vue-js-modal'
import VueCookie from 'vue-cookie'


export const backendURL = 'http://localhost:80'

Vue.use(VueCookie)
Vue.use(VModal, { dynamic: true, injectModalsContainer: true })
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


